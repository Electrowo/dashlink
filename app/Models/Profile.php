<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'plus_bio',
        'plus_discord',
        'plus_website',
        'plus_video',
        'plus_email',
        'plus_pronouns',
        'plus_location',
    ];

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function plus(): array {
        return [
            'bio' => $this->plus_bio,
            'discord' => $this->plus_discord,
            'website' => $this->plus_website,
            'video' => $this->plus_video,
            'email' => $this->plus_email,
            'pronouns' => $this->plus_pronouns,
            'location' => $this->plus_location,
        ];
    }
}
