# DashLink
OAuth 2.0 for GD

## Install
Upload these files to a webserver and point the directory index to the `public/` directory

Rename `.env.example` to `.env` and enter the required variables

Run the database migrations
```shell
php artisan migrate
```

Install passport
```shell
php artisan passport:install
```

WIP there are some missing steps like creating passport clients n stuff. This uses Laravel so that should help you google while I write up the rest of these docs.
